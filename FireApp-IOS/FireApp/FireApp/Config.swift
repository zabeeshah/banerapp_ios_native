//
//  Config.swift
//  FireApp
//
//  Created by Devlomi on 12/8/19.
//  Copyright © 2019 Devlomi. All rights reserved.
//

import UIKit

class Colors {

    static let typingAndRecordingColors = "#0080D4".toColor
    static let notTypingColor = UIColor.darkGray

    static let circularStatusUserColor = "#f2731f".toColor
    static let circularStatusSeenColor = UIColor.lightGray

    static let circularStatusNotSeenColor = UIColor.red

    static let voiceMessageSeenColor = UIColor(named: "Orange")!
    static let voiceMessageNotSeenColor = UIColor.gray

    static let chatsListIconColor = UIColor.gray

    //the default colors for read tags(pending,sent,received) in ChatVC
    static let readTagsDefaultChatViewColor = "#507f48".toColor

    static let readTagsPendingColor = UIColor.gray
    static let readTagsSentColor = UIColor.gray
    static let readTagsReceivedColor = UIColor.gray
    static let readTagsReadColor = "#f2731f".toColor

    static let replySentMsgAuthorTextColor = "#20b66e".toColor //
    static let replySentMsgBackgroundColor = "#cfe8b9".toColor // "#f7ffef".toColor
    static let sentMsgBgColor = "#dcf8c6".toColor  //b5ff94

    static let replyReceivedMsgAuthorTextColor = UIColor(named: "Orange")
    
    static let replyReceivedMsgBackgroundColor = "#f1f1f1".toColor
    static let receivedMsgBgColor = UIColor.white

    static let highlightMessageColor = UIColor.yellow
}

class TextStatusColors {
    public static let colors = [
        "#FF8A8C",
        "#54C265",
        "#8294CA",
        "#A62C71",
        "#90A841",
        "#C1A03F",
        "#792138",
        "#AE8774",
        "#F0B330",
        "#B6B327",
        "#C69FCC",
        "#8B6990",
        "#26C4DC",
        "#57C9FF",
        "#74676A",
        "#5696FF"
    ]
}


class Config {
    static let maxVideoStatusTime: Double = 30.0
    static let maxVideoTime: Double = 30.0

    static let appName = "BanerApp"

    static let bundleName = "com.fastpro.chat"
    static let groupName = "group.\(bundleName)"
    private static let teamId = "6Z2HC3NPPT"

    private static let shareURLScheme = "FireAppShare"
     static let groupVoiceCallLink = "FireAppCall"
    static let shareUrl = "\(shareURLScheme)://dataUrl=Share"

    static let groupHostLink = ""

    private static let appId = "1553826798"// you can get it from AppStore Connect
    static let appLink = "https://apps.apple.com/app/id\(appId)"

    static let sharedKeychainName = "\(teamId).\(bundleName).\(groupName)"

    static let privacyPolicyLink = "https://banerclub.com"
    
    static let agoraAppId = "d604e88f8a9b45d29c636ba2c46a0e94"
    
    //Ads Disable/Enable
    static let isChatsAdsEnabled = false
    static let isCallsAdsEnabled = false
    static let isStatusAdsEnabled = false

    //Ads Units IDs
    static let mainViewAdsUnitId = "ca-app-pub-3940256099942544/2934735716"//this is a Test Unit ID, replace it with your AdUnitId

    //About
    static let twitter = ""
    static let website = "https://banerclub.com"
    static let email = ""

    public static let MAX_GROUP_USERS_COUNT = 50
    public static let MAX_BROADCAST_USERS_COUNT = 100
    public static let maxGroupCallCount = 11

}



fileprivate extension String {

    var toColor: UIColor {
        var cString: String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

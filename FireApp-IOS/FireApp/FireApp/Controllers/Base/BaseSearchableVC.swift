//
//  BaseSearchableVC.swift
//  FireApp
//
//  Created by Devlomi on 1/2/20.
//  Copyright © 2020 Devlomi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BaseSearchableVC: BaseVC, Searchable {
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!

    @IBOutlet weak var titleLabel: UILabel!
    
    var searchController: UISearchController!

    private var initialTableViewBottomConstraint: CGFloat = 0

    var tableViewPadding: CGFloat = 20

    var enableAds = false
    var adUnitAd = "ca-app-pub-3940256099942544/2934735716"

    fileprivate func loadAd() {
        bannerView.adUnitID = adUnitAd
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        listenForKeyboard = true

        if enableAds {
            loadAd()
        } else {
            if bannerView != nil {
                bannerView.translatesAutoresizingMaskIntoConstraints = false
                bannerView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            }
        }
    }

    func setupSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        
        searchController.searchBar.searchBarStyle = .minimal
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.tintColor = .lightGray
        
//        searchController.searchBar.frame = CGRect(x:0, y: -15, width: searchController.view.frame.size.width - 20, height: searchController.searchBar.frame.size.height + 30)
    }

    override func keyboardWillShow(keyboardFrame: CGRect?) {
        if let frame = keyboardFrame {
            let percent:CGFloat = enableAds ? 1.6 : 1.3
            tableViewBottomConstraint.constant = (frame.height / percent)
        }
        if let titleLabel = titleLabel {
            titleLabel.isHidden = true
        }
    }

    override func keyBoardWillHide() {
        tableViewBottomConstraint.constant = initialTableViewBottomConstraint
        if let titleLabel = titleLabel {
            titleLabel.isHidden = false
        }

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if initialTableViewBottomConstraint == 0 {
            initialTableViewBottomConstraint = tableViewBottomConstraint.constant
        }
    }

}

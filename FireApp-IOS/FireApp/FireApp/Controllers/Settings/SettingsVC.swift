//
//  SettingsVC.swift
//  FireApp
//
//  Created by Devlomi on 11/16/19.
//  Copyright © 2019 Devlomi. All rights reserved.
//

import UIKit

enum SettingItemType: String {
    case profile = "Profile"
    case chat = "Chat"
    case notifications = "Notifications"
    case scheduledMessages = "Scheduled Messages"
    case privacyPolicy = "Privacy Policy"
    case about = "About"
    
    func title() -> String {
        return rawValue
    }
    func icon() -> UIImage? {
        switch self {
        case .profile:
            return UIImage(named: "profile")
        case .chat:
            return UIImage(named: "chat")
        case .notifications:
            return UIImage(named: "notifications")
        case .scheduledMessages:
            return UIImage(named: "pending")
        case .privacyPolicy:
            return UIImage(named: "policy")
        case .about:
            return UIImage(named: "info")
        }
    }
}

class SettingsVC: BaseVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
   
    var user: User!
    var settingItems: [[SettingItemType]] = [[.profile], [.chat, .notifications, .scheduledMessages], [.about, .privacyPolicy]]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib.init(nibName: "TableViewHeader", bundle: .main), forHeaderFooterViewReuseIdentifier: "TableViewHeader")

        user = RealmHelper.getInstance(appRealm).getUser(uid: FireManager.getUid())
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.navigationItem.title = nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settingItems.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView =  tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeader") as! TableViewHeader
        headerView.backgroundColor = UIColor.init(red: 235.0/255.0, green: 235.0/255.0, blue: 241.0/255.0, alpha: 1.0)
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingItems[section].count
    }
    //fix for a tint color bug on IOS 12
    func tableView( _ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingItemCell") as! SettingItemCell
        
        cell.iconImageView.image = settingItems[indexPath.section][indexPath.row].icon()
        cell.titleLabel.text = settingItems[indexPath.section][indexPath.row].title()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let settingItemType = settingItems[indexPath.section][indexPath.row]
        switch settingItemType {
        case .profile:
            performSegue(withIdentifier: "toProfile", sender: nil)
        case .notifications:
            performSegue(withIdentifier: "toNotifications", sender: nil)
        case .chat:
            performSegue(withIdentifier: "toChatSettings", sender: nil)
        case .privacyPolicy:
            if let url = URL(string: Config.privacyPolicyLink) {
                       UIApplication.shared.open(url)
            }
        case .scheduledMessages:
            performSegue(withIdentifier: "toScheduledMessages", sender: nil)
        case .about:
            performSegue(withIdentifier: "toAboutSettings", sender: nil)
        default:
            break
        }
    }
}

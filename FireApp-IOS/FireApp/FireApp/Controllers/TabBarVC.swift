//
//  TabBarVC.swift
//  FireApp
//
//  Created by Devlomi on 9/18/19.
//  Copyright © 2019 Devlomi. All rights reserved.
//

import UIKit
class TabBarVC: UITabBarController {


    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layoutIfNeeded()

        if !UserDefaultsManager.userDidLogin() {
            NotificationCenter.default.post(name: Notification.Name("userDidLogin"), object: nil)
            //request permissions for first time
            Permissions.requestContactsPermissions(completion: nil)
        }
        
        selectedIndex = 2
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.navigationItem.title = " "
    }
    
    func goToUsersVC() {
        performSegue(withIdentifier: "toUsersVC", sender: nil)
    }
    
    func goToGroupUsersVC(isBroadCast: Bool) {
        performSegue(withIdentifier: "tabbarToGroupUsersVC", sender: isBroadCast)
    }

    func segueToChatVC(user:User) {
          performSegue(withIdentifier: "toChatVC", sender: user)
      }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let controller = segue.destination as? UsersVCNavController {
            controller.navigationDelegate = self
        } else if let controller = segue.destination as? ChatViewController, let user = sender as? User {
            controller.initialize(user: user,delegate: self)
        }
        
        else if let controller = segue.destination as? GroupUsersVC {
            let isBroadcast = sender as! Bool
            controller.initialize(mode: .create, isBroadcast: isBroadcast, delegate: self)
        }
    }
    
   
}

extension TabBarVC: DismissViewController {
    func presentCompletedViewController(user: User) {
       goToChatVC(user: user)
    }
}


extension TabBarVC: ChatVCDelegate {
    func goToChatVC(user:User){
        segueToChatVC(user: user)
    }
}



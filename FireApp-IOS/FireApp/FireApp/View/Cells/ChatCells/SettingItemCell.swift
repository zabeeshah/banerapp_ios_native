//
//  SettingItemCell.swift
//  FireApp
//
//  Created by Mutee Ur Rehman on 09/08/2021.
//  Copyright © 2021 Devlomi. All rights reserved.
//

import UIKit

class SettingItemCell: UITableViewCell {
    @IBOutlet weak var titleLabel: LocalizableLabel!
    @IBOutlet weak var iconImageView: UIImageView!
}

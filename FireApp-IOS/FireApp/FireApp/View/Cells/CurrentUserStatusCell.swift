//
//  CurrentUserStatusCell.swift
//  FireApp
//
//  Created by Mutee Ur Rehman on 03/08/2021.
//  Copyright © 2021 Devlomi. All rights reserved.
//

import UIKit

class CurrentUserStatusCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var statusTimeLbl: UILabel!

    @IBOutlet weak var cardView: UIView!

    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnTextStatus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!

    @IBOutlet weak var textStatusLbl: UILabel!
    @IBOutlet weak var circularStatusView: CircularStatusView!

    @IBOutlet weak var buttonsStack: UIStackView!
    
    override func awakeFromNib() {
        textStatusLbl.clipsToBounds = true
        textStatusLbl.layer.cornerRadius = textStatusLbl.frame.width / 2
    }
}

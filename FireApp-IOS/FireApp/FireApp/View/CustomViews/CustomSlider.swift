//
//  CustomSlider.swift
//  FireApp
//
//  Created by Devlomi on 8/23/19.
//  Copyright © 2019 Devlomi. All rights reserved.
//

import UIKit
class CustomSlider: UISlider {
    
    @IBInspectable var trackHeight: CGFloat = 3
    
    @IBInspectable var thumbCornerRadius: CGFloat = 0
    @IBInspectable var thumbHeight: CGFloat = 16
    @IBInspectable var thumbWidth: CGFloat = 3

    // Custom thumb view which will be converted to UIImage
    // and set as thumb. You can customize it's colors, border, etc.
    private lazy var thumbView: UIView = {
        let thumb = UIView()
        thumb.backgroundColor = UIColor(named: "Orange")//thumbTintColor
//        thumb.tintColor = .orange
//        thumb.layer.borderWidth = 0.4
//        thumb.layer.borderColor = UIColor.darkGray.cgColor
        return thumb
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let thumb = thumbImage(cornerRadius: thumbCornerRadius)
        setThumbImage(thumb, for: .normal)
        setThumbImage(thumb, for: .highlighted)
        isContinuous = true
        minimumTrackTintColor = .orange
        
    }
    
    private func thumbImage(cornerRadius: CGFloat) -> UIImage {
        // Set proper frame
        // y: radius / 2 will correctly offset the thumb
        
        thumbView.frame = CGRect(x: 0, y: thumbHeight / 2, width: thumbWidth, height: thumbHeight)
        thumbView.layer.cornerRadius = cornerRadius
        
        // Convert thumbView to UIImage
        // See this: https://stackoverflow.com/a/41288197/7235585
        
        let renderer = UIGraphicsImageRenderer(bounds: thumbView.bounds)
        return renderer.image { rendererContext in
            thumbView.layer.render(in: rendererContext.cgContext)
        }
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        // Set custom track height
        // As seen here: https://stackoverflow.com/a/49428606/7235585
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }
    
}


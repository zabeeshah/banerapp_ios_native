//
//  TableViewHeader.swift
//  FireApp
//
//  Created by Mutee Ur Rehman on 03/08/2021.
//  Copyright © 2021 Devlomi. All rights reserved.
//

import UIKit

class TableViewHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var title: UILabel!
}

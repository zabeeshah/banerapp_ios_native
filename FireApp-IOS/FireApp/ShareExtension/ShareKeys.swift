//
//  ShareKeys.swift
//  ShareExtension
//
//  Created by Devlomi on 12/17/19.
//  Copyright © 2019 Devlomi. All rights reserved.
//

import Foundation
class ShareKeys {
    public static let filesPathsKey = "shared_filePaths"
    public static let vcardsPaths = "shared_vcards"
    public static let textOrUrlKey = "shared_text"
    public static let usersIdsKey = "usersIds"




}
